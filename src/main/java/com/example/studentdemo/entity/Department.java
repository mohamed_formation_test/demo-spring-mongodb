package com.example.studentdemo.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Department {


    @Field(name ="department_name")
    private String departmentName;
    private String localisation;




}
