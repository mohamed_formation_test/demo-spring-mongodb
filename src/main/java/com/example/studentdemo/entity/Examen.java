package com.example.studentdemo.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Examen {


    @Field(name ="sujet_name")
    private String sujetName;

    @Field(name ="note_sujet")
    private Integer noteSujet;




}
