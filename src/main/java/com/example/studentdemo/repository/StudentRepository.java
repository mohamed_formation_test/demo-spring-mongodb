package com.example.studentdemo.repository;

import com.example.studentdemo.entity.Student;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentRepository extends MongoRepository<Student, String> {

// Permet de récupérer des étudiants par le nom du département en se basant sur la syntaxe de la requête.
// Doc officielle sur le sujet : https://docs.spring.io/spring-data/jpa/docs/current/reference/html/
    List<Student> findByDepartmentDepartmentName(String deptName);

    Student findByNameOrEmail(String name, String email);



}
