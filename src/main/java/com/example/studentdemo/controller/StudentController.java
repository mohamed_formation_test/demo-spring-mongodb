package com.example.studentdemo.controller;


import com.example.studentdemo.entity.Student;
import com.example.studentdemo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/student")
public class StudentController {


    @Autowired
    StudentService studentService;

    @PostMapping("/create")
    public Student createStudent(@RequestBody Student student){
        return studentService.createStudent(student);
    }
    @GetMapping("/all")
    public List<Student> get() {
        return studentService.getAllStudents();
    }

    @GetMapping("/get/{id}")
    public Student getStudentById(@PathVariable String id) throws Exception {
        return studentService.getStudentById(id);
    }

    @PutMapping("/update")
    public Student update(@RequestBody Student student) {
        return studentService.updateStudent(student);
    }

    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable String id) {
        return studentService.deleteStudent(id);
    }


    @GetMapping("/byDepartment")
    public List<Student> getStudentsDepartmentName(@RequestParam String deptName){
        return studentService.getStudentsByNameDept(deptName);
    }


}
